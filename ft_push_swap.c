#include "push_swap.h"

int ft_validate(char *str)
{
	int i;

	i = 0;
	if (!str)
		return (0);
	while (str[i] != '\0')
	{
		if (str[i] < '0' || str[i] > '9')
		{
			if (str[i] != '-')
				return (0);
		}
		i++;
	}
	return (1);
}

int in_order(int amtA,int *listA)
{
	int i;
	int tmp;

	i = 0;
	tmp = listA[i];
	while (i < amtA)
	{
		if (tmp > listA[i])
		{
			if (i == amtA - 2)
				return (2);
			return (0);
		}
		tmp = listA[i];
		i++;
	}
	return (1);

}

void ts_print(int amt,int *listA)
{
	int i;

	i = 0;
	while (i < (amt))
	{
		ft_putnbr(listA[i]);
		ft_putchar(' ');
		i++;
		
	}
	printf("\n\n");
	return ; 
}

void ft_push_swap(int amtA, int *listA)
{
	int *listB;
	int amtB;
	int moves;

	moves = 0;
	if (!(listB = (int *)malloc(sizeof(int) * amtA)))
		return ;
	amtB = 0;
	
	if (amtA < 10 && listA[0] <= listA[1])
		small_sort(listA, amtA, &moves);
	else if (in_order(amtA, listA) == 2)
		small_sort(listA, amtA, &moves);
	else
		my_sort(listA, listB, amtA, &moves);
	free(listB);
	listB = NULL;
	ft_putstr("\nMoves needed: ");
	ft_putnbr(moves);	
	return ;
}

int main(int argc, char **argv)
{
	int i;
	int *listA;
	int j;

	j = 0;
	i = 1;
	if (argc < 2)
	{
		ft_putstr("\nMoves needed: 0");
		return (0);
	}
	if (!(listA = (int *)malloc(sizeof(int) * argc - 1)))
		return (0);
	while (i < argc)
	{	
		if(ft_validate(argv[i]) == 0)
		{
			ft_putstr("Error\n");
			free(listA);
			listA = NULL;
			return (0);
		}
		listA[j++] = ft_atoi(argv[i++]);
	}
	ft_push_swap(j, listA);
	free(listA);
	listA = NULL;
	ft_putstr("\nInputs taken: ");
	ft_putnbr(argc - 1);		
	ft_putstr("\n");
	
	return (0);
}