#include "push_swap.h"

void 		my_sort(int *listA, int*listB, int amtA, int *moves)
{
	int trn;
	int i;
	int amtB;
	int tmp;
	int k;
	int revrot;

	amtB = 0;
	tmp = amtA;
	if(in_order(amtA, listA) == 1)
	{
		ts_print(amtA, listA);
		return ;
	}
	while (amtB < tmp)
	{
		k = 0;
		i = 0;
		trn = listA[i];
		revrot = 0;
		while (i < amtA)
		{
			if (listA[i] < trn)
			{
				trn = listA[i];
				k = i;
			}
			i++;
		}
		if (k <= amtA / 2)
			revrot = 1;
		while (listA[amtA - 1] != trn)
		{
			if (revrot == 1)
			{
				listA = ft_rev_rot(amtA, listA);
				ft_putstr("RRA ");
				(*moves)++;
			}
			if (revrot == 0)
			{
				listA = ft_rotate_list(amtA, listA);
				ft_putstr("RA ");
				(*moves)++;
			}
		}
		ft_push_list(listA, &amtA, listB, &amtB);
		ft_putstr("PB ");
		(*moves)++;
	}
	while (amtB != 0)
	{
		listB = ft_rev_rot(amtB, listB);
		ft_putstr("RRB ");
		(*moves)++;
		ft_push_list(listB, &amtB, listA, &amtA);
		ft_putstr("PA ");
		(*moves)++;
	}
	ft_putstr("\n\n");
	if(in_order(amtA, listA) == 1)
		ts_print(amtA, listA);
}