#include "push_swap.h"

void 		small_sort(int *listA, int amtA, int *moves)
{
	while (in_order(amtA, listA) == 0)
	{
		if (listA[amtA - 2] > listA[amtA - 1])
		{
			listA = ft_swap_list(amtA, listA);
			(*moves)++;
			ft_putstr("SA ");
		}
		else
		{
			listA = ft_rev_rot(amtA, listA);
			(*moves)++;
			ft_putstr("RRA ");
		}
	}
	ft_putstr("\n\n");
	ts_print(amtA, listA);

}

//Change this to a sorting algorithm made for small data sets