#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include "libft.h"

int 		*ft_rotate_list(int amt, int *list);
int 		*ft_swap_list(int amt, int *list);
void 		ft_push_list(int *list1, int *amt1, int *list2, int *amt2);
int 		*ft_rev_rot(int amt, int *list);
void 		ft_selection_sort(int *listA, int*listB, int amt);
void 		ts_print(int amt,int *listA);
void 		ft_quick_sort(int *listA, int*listB, int amtA, int *moves);
void 		my_sort(int *listA, int*listB, int amtA, int *moves);
int 		in_order(int amtA,int *listA);
void 		small_sort(int *listA, int amtA, int *moves);


#endif