#include "push_swap.h"


//RA: rotates the A list, making the first element the last
//pushing everything to the right so to speak
//if empty does nothing CODED
//RB: same concept as RA but for B CODED
int *ft_rotate_list(int amt, int *list)
{
	int *new;
	int i;//received list
	int k;//new list

	i = amt - 1;
	k = 0;
	if(!(new = (int *)malloc(sizeof(int) * amt)))
		return (0);//mallocing the new rotated list
	new[k] = list[i];//setting the last elem to the first
	k++;//advancing further in new list
	i = 0;//
	while (k < amt)
	{
		new[k] = list[i];
		k++;
		i++;
	}
	return (new);
}

//RRA: same convept as RA but in reverse
//pushing everything in A to the left so to speak
//RRB: same conecpt as RRA but for B
int *ft_rev_rot(int amt, int *list)
{
	int *new;
	int i;
	int k;

	i = 0;
	k = 0;
	if(!(new = (int *)malloc(sizeof(int) * amt)))
		return (0);
	new[amt - 1] = list[i];
	i++;
	while(k < amt - 1)
	{
		new[k] = list[i];
		k++;
		i++;
	}
	return (new);
}

//PA: Takes the first element of B and puts it onto A //CODED
//PB: Takes the first element of A and puts it onto B //CODED
void ft_push_list(int *list1, int *amt1, int *list2, int *amt2)
{
	//alright so list one is going to be the one we push FROM
	//list two is the one that receives the number
	list2[(*amt2)] = list1[(*amt1) - 1];
	(*amt2)++;
	list1[(*amt1) - 1] = 0;
	(*amt1)--;
	//this function pushed one item from one list
	//to another whilst changing the elements count for each
}

// SA: This swaps the first two elements of pile A 
//if there are no elements it does nothing CODED
// SB: This swaps the first two elements of pile B 
//if there are no elements it does nothing CODED
int *ft_swap_list(int amt, int *list)
{
	int *new;
	int i; //initial list
	int k; //modded list

	i = 0;
	k = 0;
	if(!(new = (int *)malloc(sizeof(int) * amt)))
		return (0);//mallocing the new rotated list
	new[amt - 1] = list[amt - 2];
	new[amt - 2] = list[amt - 1];
	amt = amt - 2;
	while (i != amt)
	{
		new[i] = list[k];
		i++;
		k++;
	}
	return (new);
}