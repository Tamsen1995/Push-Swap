CC = gcc
FLAGS = -Wall -Wextra -Werror

LIBFT = libft.a
NAME = push_swap
SRC = ft_push_swap.c \
	  handle.c \
	  operations.c \
	  my_sort.c \
	  small_sort.c \

all: $(LIBFT) $(NAME)

$(LIBFT):
	make -C libft/

$(NAME):
	@echo "building binary file"
	@$(CC) $(FLAGS) $(SRC) -o $(NAME) -I -lft $(LIBFT)

clean:
	@make clean -C libft/

fclean: clean
	@echo "delete $(NAME)"
	@rm -f $(NAME)
	@make fclean -C libft/

re: fclean all